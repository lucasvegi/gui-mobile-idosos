package com.example.frameworkformulario.data;

import java.io.Serializable;
import java.util.ArrayList;

public class Usuario implements Serializable {

    private static final int NUMERO_DE_TELAS = 17;

    private int tamanhoLetra;
    private String sexo;
    private String nome;
    private String opIdade;
    private String opAnimal;
    private String opFreq;
    private String opCasa;
    private String opRS;
    private String opAudio;
    private String opCor1;
    private String opCor2;
    private String opCor3;

    private int telaWidth;
    private int telaHeight;

    private ArrayList<TratamentoDeTela> tela;

    public Usuario(int telaWidth, int telaHeight){
        sexo = "";
        nome = "";
        opIdade = "";
        opAnimal = "";
        opAudio = "";
        opCasa = "";
        opFreq = "";
        opRS = "";
        opCor1 = "";
        opCor2 = "";
        opCor3 = "";
        tamanhoLetra = 0;
        this.telaWidth = telaWidth;
        this.telaHeight = telaHeight;
        tela = new ArrayList<>();
        for(int i = 0; i < NUMERO_DE_TELAS; i++)
            tela.add(new TratamentoDeTela());
    }

    public int getNumeroDeTelas(){
        return NUMERO_DE_TELAS;
    }

    public int getTamanhoLetra (){
        return tamanhoLetra;
    }

    public void setTamanhoLetra (int tamanhoLetra){
        this.tamanhoLetra = tamanhoLetra;
    }

    public int getTelaWidth (){
        return telaWidth;
    }

    public void setTelaWidth (int telaWidth){
        this.telaWidth = telaWidth;
    }

    public int getTelaHeight (){
        return telaHeight;
    }

    public void setTelaHeight (int telaHeight){
        this.telaHeight = telaHeight;
    }

    public String getSexo(){
        return sexo;
    }

    public void setSexo (String sexo){
        this.sexo = sexo;
    }

    public String getNome(){
        return nome;
    }

    public void setNome (String nome){
        this.nome = nome;
    }

    public String getOpIdade(){
        return opIdade;
    }

    public void setOpIdade (String opIdade){
        this.opIdade = opIdade;
    }

    public String getOpAnimal(){
        return opAnimal;
    }

    public void setOpAnimal (String opAnimal){
        this.opAnimal = opAnimal;
    }

    public String getOpFreq(){
        return opFreq;
    }

    public void setOpFreq (String opFreq){
        this.opFreq = opFreq;
    }

    public String getOpCasa(){
        return opCasa;
    }

    public void setOpCasa (String opCasa){
        this.opCasa = opCasa;
    }

    public String getOpRS(){
        return opRS;
    }

    public void setOpRS (String opRS){
        this.opRS = opRS;
    }

    public String getOpAudio(){
        return opAudio;
    }

    public void setOpAudio (String opAudio){
        this.opAudio = opAudio;
    }

    public TratamentoDeTela getTela(int index){
        return tela.get(index);
    }

    public void setTela (TratamentoDeTela tela){
        this.tela.add(tela);
    }

    public String getOpCor1() {
        return opCor1;
    }

    public void setOpCor1(String opCor1) {
        this.opCor1 = opCor1;
    }

    public String getOpCor2() {
        return opCor2;
    }

    public void setOpCor2(String opCor2) {
        this.opCor2 = opCor2;
    }

    public String getOpCor3() {
        return opCor3;
    }

    public void setOpCor3(String opCor3) {
        this.opCor3 = opCor3;
    }
}
